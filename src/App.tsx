import React, { FC } from 'react'
import Grid from '@material-ui/core/Grid'
import Header from './components/Header'
import ArticleList from './components/ArticleList'
import articles from './articles'

const header = {
	headers: {
		"X-RapidAPI-Key": "621b3af183msh453ff9ee947c34bp1a1763jsna3da25f62c3b",
	},
}

async function getApi() {
	try {
		const response = await fetch('https://api-football-v1.p.rapidapi.com/teams/team/51', header)
		console.log(response);
	} catch (e) {
		console.log(e);
	}
}

getApi();

export const App: FC = () => {
	return (
		<div className="App">
			<Header/>
			<Grid container justify={ 'center' }>
				<Grid item xs={ 6 }>
					<ArticleList articles={ articles }/>
				</Grid>
			</Grid>
		</div>
	);
};
