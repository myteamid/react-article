import React, { FC } from 'react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Typography from '@material-ui/core/Typography'
import Comment from '../Comment'
import { ArticleCommentInterface } from "../../types";

type Props = {
  comments?: Array<ArticleCommentInterface>
}

const CommentList: FC<Props> = ({comments}) => {
  if (!comments) return <Typography component="p" align="center"> No comments </Typography>;

  const renderBody = comments.map(item =>
    <ListItem key={item.id}>
      <Comment item={item} />
    </ListItem>
  );

  return (
    <React.Fragment>
      <Typography component="h3" align="center"> Comments </Typography>
    <List>
      {renderBody}
    </List>
    </React.Fragment>
  )
};

export default CommentList
