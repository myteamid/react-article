import React, { FC, useState } from 'react'
import { withStyles, createStyles, Theme, WithStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import CommentList from '../CommentList'
import { ArticleInterface } from "../../types";

const styles = (theme: Theme) => createStyles({
	root: {
		...theme.mixins.gutters(),
		paddingTop: theme.spacing.unit * 2,
		paddingBottom: theme.spacing.unit * 2,
	},
	content: {
		marginTop: theme.spacing.unit * 2,
		fontSize: "14px"
	},
	time: {
		display: "flex",
		alignItems: "center",
		height: "100%",
		justifyContent: "flex-end"
	}
});

type OwnProps = {
	item: ArticleInterface,
	classes: Record<"root" | "content" | "time", string>
}

type Props = OwnProps & WithStyles<typeof styles>

const Article: FC<Props> = ({ item, classes }) => {
	let [ open, setOpen ] = useState(false);

	const { date, title, text } = item;

	const handleClick = () => setOpen(!open);

	const showButton = () => (
		item.comments ?
			<CardActions onClick={ handleClick }>
				<Button size="small" variant="contained">
					{ open ? "Close comments" : "Show comments" }
				</Button>
			</CardActions>
			: null
	);

	const renderComments = () => open ? <CommentList comments={ item.comments }/> : null;

	return (
		<Card className={ classes.root }>
			<CardContent>
				<Grid container>
					<Grid item md={ 8 }>
						<Typography variant="h4">
							{ title }
						</Typography>
					</Grid>
					<Grid item md={ 4 }>
						<Typography className={ classes.time } variant="caption">
							{ date }
						</Typography>
					</Grid>
				</Grid>
				<Typography className={ classes.content } variant="subheading">
					{ text }
				</Typography>
			</CardContent>

			{ renderComments() }

			{ showButton() }

		</Card>
	)
};


export default withStyles(styles)(Article);
