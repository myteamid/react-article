import React, { FC } from 'react'
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

const styles = createStyles({
	root: {
		width: '100%',
	},
});

type OwnProps = {
	classes: Record<"root", string>
}

type Props = OwnProps & WithStyles<typeof styles>

const Header: FC<Props> = ({ classes }) => {
	return (
		<AppBar position="static" color="secondary">
			<Toolbar>
				<Typography className={ classes.root }
							variant="h6"
							color="inherit"
							align="center">
					Articles
				</Typography>
			</Toolbar>
		</AppBar>
	)
};

export default withStyles(styles)(Header)
