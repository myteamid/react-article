import React, { FC } from 'react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Article from '../Article'
import { ArticleInterface } from "../../types";

type Props = {
	articles: Array<ArticleInterface>
}

const ArticleList: FC<Props> = ({ articles }) => {
	const renderBody = articles.map(item =>
		<ListItem key={ item.id }>
			<Article item={ item }/>
		</ListItem>
	);

	return (
		<List>
			{ renderBody }
		</List>
	)
};

export default ArticleList
