import React, { FC } from 'react'
import ListItemText from '@material-ui/core/ListItemText'
import { ArticleCommentInterface } from "../../types";

type Props = {
	item: ArticleCommentInterface
}
const Comment: FC<Props> = ({ item }) => (
	<ListItemText
		primary={ item.user }
		secondary={ item.text }
	>
	</ListItemText>
);

export default Comment;
