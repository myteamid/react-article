export interface ArticleInterface {
	id: string
	date: string,
	title: string,
	text: string,
	comments?: Array<ArticleCommentInterface> | undefined
}

export interface ArticleCommentInterface {
	id: string,
	user: string,
	text: string
}